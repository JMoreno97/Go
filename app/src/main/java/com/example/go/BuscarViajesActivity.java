package com.example.go;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.go.R;

public class BuscarViajesActivity extends AppCompatActivity {

    EditText etOrigen;
    EditText etDestino;
    EditText etFecha;
    EditText etHora;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.viajes_activity);


        etOrigen = (EditText) findViewById(R.id.etOrigenViaje);
        etDestino = (EditText) findViewById(R.id.etDestinoViaje);
        etFecha = (EditText) findViewById(R.id.etFechaViaje);
        etHora = (EditText) findViewById(R.id.etHoraViaje);

    }

    public void onBuscarViaje(View view) {

        Intent mostrarviajesIntent = new Intent(this, MostrarViajes.class);
        Bundle bundleViaje = new Bundle();


        bundleViaje.putString("origen", etOrigen.getText().toString());
        bundleViaje.putString("destino", etDestino.getText().toString());
        bundleViaje.putString("fecha", etFecha.getText().toString());
        bundleViaje.putString("hora", etHora.getText().toString());

        mostrarviajesIntent.putExtras(bundleViaje);
        startActivity(mostrarviajesIntent);




    }
}
