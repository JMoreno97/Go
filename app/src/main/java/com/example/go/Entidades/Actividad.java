package com.example.go.Entidades;

import android.media.Image;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Actividad {

    private String identificador;
    private String descripcion;
    private List<Image> imagenes;
    private List<Valoracion> valoraciones;


}
