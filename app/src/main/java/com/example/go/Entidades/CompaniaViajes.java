package com.example.go.Entidades;

import java.util.List;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=true)
public class CompaniaViajes extends Compania {

    private Map<String, Viaje> viajes;
    private List<Valoracion> valoraciones;
}
