package com.example.go.Entidades;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Usuario {

    private String nombreUsuario;
    private String correo;
    private String nombre;
    private String apellidos;

    private List<Necesidad> necesidades;
    private List<Viaje> viajesRealizados;
    private List<Actividad> actividadesRealizadas;


}
