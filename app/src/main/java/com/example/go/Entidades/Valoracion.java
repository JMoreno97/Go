package com.example.go.Entidades;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Valoracion {

    private int rating;
    private Usuario reviewer;

}
