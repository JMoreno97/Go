package com.example.go.Entidades;

import java.time.LocalDateTime;
import java.util.Calendar;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Viaje {


    private String identificador;
    private String origen;
    private String destino;
    private Calendar fechaDeSalida;
    private Calendar fechaDeLlegada;
}
