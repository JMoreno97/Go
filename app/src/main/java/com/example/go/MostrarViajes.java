package com.example.go;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;


import com.example.go.Entidades.Viaje;
import com.example.go.R;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MostrarViajes extends AppCompatActivity {


    List<Viaje> viajesTren;
    List<Viaje> viajesBus;
    List<Viaje> viajesTaxi;


    ListView lViajes;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mostrar_viajes_activity);

        Bundle bundleViaje = getIntent().getExtras();
        lViajes = (ListView) findViewById(R.id.listaViajes);

        String[] palabras = {"Hola", "adios", "prueba", "etc"};
        Arrays.sort(palabras);
        lViajes.setAdapter(
                new ArrayAdapter<>(
                        this,
                        android.R.layout.simple_list_item_1,
                        palabras)

        );


    }


    public void onMostrarViajesTab1(){

        lViajes.setAdapter(
                new ArrayAdapter<>(
                this,
                android.R.layout.simple_list_item_1,
                viajesTaxi)

        );

    }

    public void onMostrarViajesTab2(){

        lViajes.setAdapter(
                new ArrayAdapter<>(
                        this,
                        android.R.layout.simple_list_item_1,
                        viajesBus)

        );
    }

    public void onMostrarViajesTab3(){

        lViajes.setAdapter(
                new ArrayAdapter<>(
                        this,
                        android.R.layout.simple_list_item_1,
                        viajesTren)

        );

    }



    private void obtenerViajes(Bundle bundleViaje, String medioTransporte){

        String origen = bundleViaje.get("origen").toString();
        String destino = bundleViaje.get("destino").toString();
        String fecha = bundleViaje.get("fecha").toString();
        String hora = bundleViaje.get("hora").toString();

        /*
         * Simular llamada a BD.
         *
         */

        Calendar rightNow = Calendar.getInstance();
        switch(medioTransporte){
            case "Taxi":
                viajesTaxi.add(new Viaje(medioTransporte,origen,destino, rightNow,rightNow));
                break;
            case "Bus":
                viajesBus.add(new Viaje(medioTransporte,origen,destino, rightNow,rightNow));

                break;
            case "Tren":
                viajesTren.add(new Viaje(medioTransporte,origen,destino, rightNow,rightNow));

                break;
        }

    }

}
